//发送post请求，出来post请求参数
import { baseTableList, tongjiList, chartsInfo,treeList } from "./mock.js";

// 引入: mockjs 模块;
import Mock from 'mockjs'

// 内存模拟数据
const arr = []
const total = 200;
const chatDta = [];

// 循环 total 次，push total 个对象;
for (let i = 0; i < 50; i++) {
  arr.push({
    id: Mock.mock('@guid'),
    name: Mock.mock('@cname'),
    sex: Mock.Random.integer(0, 1),
    status: Mock.Random.integer(0, 1),
    phone: "13011111111",
    createTime: Mock.mock('@datetime()')
  })
}

for (let i = 0; i < 7; i++) {
  chatDta.push(Mock.Random.integer(10, 400))
}


//1.引入express模块
// const express = require('express')
import express from "express";
//2.创建服务器
const app = express();
//3.发送请求或者参数
/**
 * 3.1.通过 req.query 对象，发送带参的POST请求：
 */
// app.post('/page',(req,res) => {
//   res.send(req.query)
// })

//3.2.通过 req.params 对象，发送带出的POST请求：
// app.post('/page/:id',(req,res) => {
//   res.send(req.params)
// })
//3.3.通过绑定两个事件，来获取请求带的参数
app.post("/login", (req, res) => {
  let data = "";
  req.on("data", function (chunk) {
    data += chunk;
  });
  req.on("end", function () {
    try {
      let _data = JSON.parse(data);
      if (_data.name !== "admin" || _data.password !== "123456") {
        throw new Error("账号或者密码错误");
      }
      res.send({
        code: 0,
        msg: "登录成功~",
        data: _data,
      });
    } catch (error) {
      res.send({
        code: -1,
        msg: "登录失败",
        error: error.message,
      });
    }
  });
});

// 退出
app.get("/loginout", (req, res) => {
  res.send({
    code: 0,
    msg: "退出登录成功~",
    data: null,
  });
});

app.get("/table/list", (req, res) => {
  res.send({
    code: 0,
    msg: "请求成功~",
    data: baseTableList,
  });
});

app.delete("/user/delete", (req, res) => {
  res.send({
    code: 0,
    msg: "删除成功~"
  });
});

app.post("/user/add", (req, res) => {
  res.send({
    code: 0,
    msg: "添加成功~"
  });
});

app.post("/user/edit", (req, res) => {
  res.send({
    code: 0,
    msg: "编辑成功~"
  });
});

//用户列表
app.get("/user/list", (req, res) => {
  // let {page,pageSize,name} = req.query
  res.send({
    code: 0,
    msg: "请求成功~",
    data: arr,
    total: total
  });
});

//面板列表
app.get("/data/list", (req, res) => {
  res.send({
    code: 0,
    msg: "请求成功~",
    data: tongjiList,
  });
});

//面板对应的数据
app.post("/data/charts", (req, res) => {
  res.send({
    code: 0,
    msg: "编辑成功~",
    // data: chartsInfo[Mock.Random.integer(0, 4)]
    // data: [{
    //   series:chartsInfo[Mock.Random.integer(0, 4)],
    //   xAxis:['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00',]
    // }]
    data: {
      series: chartsInfo[Mock.Random.integer(0, 4)],
      xAxis: ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00',]
    }
  });
});

//饼状图的返回数据
app.get("/data/chartaa", (req, res) => {
  res.send({
    code: 0,
    msg: "请求成功~",
    data: chatDta,
  });
});

//面板列表
app.get("/dept/list", (req, res) => {
  res.send({
    code: 0,
    msg: "请求成功~",
    data: treeList,
  });
});


//4.启动服务器
app.listen("9000", () => {
  console.log("启动成功");
});
