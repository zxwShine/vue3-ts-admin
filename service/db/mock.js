//基本表格数据
export const baseTableList = [
  { date: '2023-08-11', pv: 11862, order: 836 },
  { date: '2023-08-10', pv: 15261, order: 827 },
  { date: '2023-08-09', pv: 16352, order: 1738 },
  { date: '2023-08-08', pv: 8632, order: 783 },
  { date: '2023-08-07', pv: 73813, order: 133 },
  { date: '2023-08-06', pv: 8319, order: 153 },
  { date: '2023-08-05', pv: 17362, order: 143 },
  { date: '2023-08-04', pv: 3522, order: 342 },
  { date: '2023-08-03', pv: 13628, order: 524 },
  { date: '2023-08-02', pv: 16382, order: 424 },
  { date: '2023-08-01', pv: 16372, order: 243 },
  { date: '2023-07-31', pv: 23234, order: 423 },
  { date: '2023-07-30', pv: 18363, order: 252 },
  { date: '2023-07-29', pv: 18393, order: 135 },
]

export const tongjiList = [{
  key: 'pv',
  label: '浏览量',
  value: '10,000',
  rate: '10.97%'
},
{
  key: 'mv',
  label: '访客数',
  value: '4,700',
  rate: '16.87%'
},
{
  key: 'order',
  label: '支付数',
  value: '435',
  rate: '4.72%'
},
{
  key: 'orderAmt',
  label: '交易金额',
  value: '198,262.87',
  rate: '26.86%'
},
{
  key: 'orderRto',
  label: '转化率',
  value: '37.68%',
  rate: '-6.86%'
}]

export const chartsInfo = [
  [10392, 6252, 8362, 9224, 6252, 9274, 2484],
  [8362, 4321, 5647, 7642, 4527, 8735, 2010],
  [182, 262, 612, 56, 816, 177, 350],
  [9273.73, 9889.98, 182738.08, 145.97, 158273.90, 8273.74],
  [0.76, 0.7526, 0.8723, 0.2413, 0.6241, 0.5482, 0.8721]
]

export const treeList = [
  [
    {
      id: 1,
      label: '计算机学院',
      children: [
        {
          id: 4,
          label: '计算机科学与技术',
          children: [
            {
              id: 9,
              label: '22-计科-1',
            },
            {
              id: 10,
              label: '22-计科-2',
            },
            {
              id: 11,
              label: '22-计科-3',
            },
            {
              id: 12,
              label: '22-计科-4',
            },
          ],
        },
      ],
    },
    {
      id: 2,
      label: '电子信息工程学院',
      children: [
        {
          id: 5,
          label: '电子信息1',
        },
        {
          id: 6,
          label: '电子信息2',
        },
      ],
    },
    {
      id: 3,
      label: '舞蹈与艺术学院',
      children: [
        {
          id: 7,
          label: '音乐教育1',
        },
        {
          id: 8,
          label: '音乐教育2',
        },
      ],
    },
  ]
]