import { createApp } from 'vue'
import './style.css'
import router from './router'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'dayjs/locale/zh-cn';
import locale from 'element-plus/es/locale/lang/zh-cn';

//pinia
import { createPinia } from 'pinia' 
const pinia = createPinia()

const app = createApp(App)
app.use(ElementPlus,{locale})
app.use(router)
app.use(pinia)
app.mount('#app')
