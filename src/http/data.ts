import request from "./index";

/**
 * @description 获取面板数据信息 
 * @params data
 */
export const getOverviewInfo = () => {
    return request<USER.userlist>({
        url: "/api/data/list", // mock接口
        method: "get",
    });
}


/**
* @description 获取面板对应的当日数据信息 
* @params data
*/
export const getCharts = (data: any) => {
    return request<USER.dataType>({
        // url: "/api/data/charts/"+data, // mock接口,这样写，参数是放在协议里的，后台从协议里获取，具体如何写，看后台接口
        url: "/api/data/charts", // mock接口,这样写，参数是放在协议里的，后台从协议里获取，具体如何写，看后台接口
        method: "post",
        data
    });
}

/**
* @description 获取饼状图数据
* @params data
*/
export const getChartAA = (data: any) => {
    return request<USER.userlist>({
        // url: "/api/data/charts/"+data, // mock接口,这样写，参数是放在协议里的，后台从协议里获取，具体如何写，看后台接口
        url: "/api/data/chartaa", // mock接口,这样写，参数是放在协议里的，后台从协议里获取，具体如何写，看后台接口
        method: "get",
        params: data//get方式传参，具体如何传参，看后台设定
    });
}
