import type { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";
import axios from "axios";
import { ElMessage } from "element-plus";
import "element-plus/es/components/message/style/css";

interface statusCode {
  [propName: number]: string;
}
const statusCode: statusCode = {
  200: "服务器成功返回请求的数据",
  201: "新建或修改数据成功。",
  202: "一个请求已经进入后台排队（异步任务）",
  204: "删除数据成功",
  400: "请求错误(400)",
  401: "未授权，缺少令牌",
  403: "拒绝访问(403)",
  404: "请求出错(404)",
  408: "请求超时(408)",
  500: "服务器错误(500)",
  501: "服务未实现(501)",
  502: "网络错误(502)",
  503: "服务不可用(503)",
  504: "网络超时(504)",
};
const http = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 10000,
  withCredentials: false,
});

interface UseAxiosRequestConfig extends AxiosRequestConfig {
  interceptors?: AxiosRequestConfig;
  headers: any;
}

// 请求拦截器
http.interceptors.request.use(
  (config: UseAxiosRequestConfig) => {
    // todo  我们可以把协议中的api放在这里添加到其他协议前面
    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  }
);

// 响应拦截器
http.interceptors.response.use(
  (response: AxiosResponse) => {
    // todo
    return response;
  },
  (error: AxiosError) => {
    const response = Object.assign({}, error.response);
    response &&
      ElMessage.error(
        statusCode[response.status] || "系统异常, 请检查网络或联系管理员！"
      );
    return Promise.reject(error);
  }
);

interface responseData<T> {
  code: number;
  data: T;
  msg: string;
}

const request = <T = unknown>(
  config: AxiosRequestConfig
): Promise<responseData<T>> => {
  return new Promise((resolve, reject) => {
    http
      .request<T>(config)
      .then((res: AxiosResponse) => resolve(res.data))
      .catch((err: { message: string }) => reject(err));
  });
};

export default request;
