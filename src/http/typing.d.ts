declare namespace USER {
  //user 
  type userInfoReq = {
    name: string;
    status: number;
    createTime: string;
    page: number;
    pageSize: number;
  };
  type userInfoRes = {
    code: number;
    msg: string;
    total: number;
    data: Array
  }[];
  // user——table
  type userlist = {
    code: number;
    msg: string;
    data: Array
  }[];
  // data 面板信息——大多数相同的，可以定义一个公共类  这里是返回一个数组类型的数据
  // type dataType = {
  //   code: number;
  //   msg: string;
  //   data: Array
  // }[];
  //这里是返回一个对象类型的数据
  type dataType = {
    code: number;
    msg: string;
    data: Object
  };
  // Login
  type loginReq = {
    password: string;
    name: string;
  };
  type commonRes = {
    code: number;
    msg: string;
  };

  interface Page {
    page: number;
    total: number;
    pageSize: number;
  }
}
