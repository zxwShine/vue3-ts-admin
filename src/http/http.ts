import request from "./index";
/**
 * @description: 用户登录
 * @params data
 */
export const loginAPI = (data: USER.loginReq) => {
  return request<USER.commonRes>({
    url: "/api/login", // mock接口
    method: "post",
    data,
    headers: {
      "content-type": "application/json;charset=utf-8",
    },
  });
};

/**
 * @description: 退出登录
 * @params data
 */
export const loginout = () => {
  return request({
    url: "/api/loginout", // mock接口
    method: "get",
  });
};

/**
 * @description 获取用户信息 
 * @params data
 */
export const getUserList = (data: USER.userInfoReq)=>{
  return request<USER.userInfoRes>({
    url: "/api/user/list", // mock接口
    method: "get",
    data,
  });
}

//删除用户
export const deleteUser = (data: any) => {
  return request<USER.commonRes>({
    url: "/api/user/delete", // mock接口
    method: "delete",
    data,
  });
};

//新增用户
export const addUser = (data: any) => {
  return request<USER.commonRes>({
    url: "/api/user/add", // mock接口
    method: "post",
    data,
  });
};

//编辑用户
export const editUser = (data: any) => {
  return request<USER.commonRes>({
    url: "/api/user/edit", // mock接口
    method: "post",
    data,//这样写，参数是放在body中的
  });
};


/**
 * @description: 基本表格数据
 * @params data
 */
export const getTableList = () => {
  return request<USER.userlist>({
    url: "/api/table/list", // mock接口
    method: "get",
  });
};


/**
 * @description: 部门数据
 * @params data
 */
export const getDeptList = () => {
  return request<USER.userlist>({
    url: "/api/dept/list", // mock接口
    method: "get",
  });
};
