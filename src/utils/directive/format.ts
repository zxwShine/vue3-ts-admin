import { Directive, DirectiveBinding } from "vue";
const formatNumber: Directive = {
  mounted(el, binding: DirectiveBinding) {
    const value = binding.value.text || "0";
    const parsedValue = parseFloat(value);

    if (!isNaN(parsedValue)) {
      const formattedValue = parsedValue.toLocaleString("en-US", {
        minimumFractionDigits: 0,
        maximumFractionDigits: parsedValue % 1 === 0 ? 0 : binding.value.digit, // 保留n位小数
      });

      el.innerText = formattedValue;
    }
  },
};

export default formatNumber;