

export const menuList = () => {
  return [
    {
      path: 'home',
      url: '/home',
      title: '数据中心',
      icon: 'icon-dashboard'
    },
    {
      path: 'user',
      url: '/user',
      title: '用户管理',
      icon: 'icon-dashboard'
    },
    {
      path: 'table',
      url: '/table',
      title: '表格',
      icon: 'icon-dashboard',
      children:[
        {
          path: 'table',
          url: '/table/search',
          title: '搜索表格',
          icon: 'icon-dashboard'
        },
        {
          path: 'base',
          url: '/table/base',
          title: '基本表格',
          icon: 'icon-dashboard'
        },
      ]
    },
    {
      path: 'charts',
      url: '/charts',
      title: '图表',
      icon: 'icon-dashboard',
      children:[
        {
          path: 'bar',
          url: '/charts/bar',
          title: '柱状图',
          icon: 'icon-dashboard'
        },
        {
          path: 'line',
          url: '/charts/line',
          title: '折线图',
          icon: 'icon-dashboard'
        },
      ]
    },
  ]
}

