import { createRouter,RouteRecordRaw,createWebHistory } from "vue-router";
import layout from '../components/layout.vue'
import login from '../views/Login.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login',
    component: () =>
          import( /* webpackChunkName: "login" */ '../views/Login.vue'),
  },
  {
    path: '/login',
    //redirect: '/login',
    component: login,
  },
  {
    path: '/',
    component:  layout,
    redirect: '/',
    children:[
      {
        path:'home',
        name: 'home',
        component: () => import('../views/Home.vue')
      }
    ]
  },
  {
    path: '/',
    component:  layout,
    redirect: '/',
    children:[
      {
        path:'user',
        name: 'user',
        component: () => import('../views/User.vue')
      }
    ]
  },
  {
    path:'/table',
    component:  layout,
    redirect: '/table/search',
    children: [
      {
        path:'/table/search',
        name: 'search',
        component: () => import('../views/table/SearchTable.vue')
      },
      {
        path:'/table/base',
        name: 'base',
        component: () => import('../views/table/BaseTable.vue')
      },
    ]
  },
  {
    path:'/charts',
    component:  layout,
    redirect: '/charts/bar',
    children: [
      {
        path:'/charts/bar',
        name: 'bar',
        component: () => import('../views/charts/Bar.vue')
      },
      {
        path:'/charts/line',
        name: 'line',
        component: () => import('../views/charts/Line.vue')
      },
    ]
  },
]


const router = createRouter({
  history: createWebHistory(''),
  routes,

})
export default router