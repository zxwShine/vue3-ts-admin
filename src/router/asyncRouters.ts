/**
 * 模拟请求后的路由数据
 */

declare interface AsyncRoutesType {
  path: string,
  url: string,
  title: string,
  icon: string,
  children?: AsyncRoutesType[]
}

export const asyncRoutes: AsyncRoutesType[] = [
  {
    path: 'home',
    url: '/home',
    title: '数据中心',
    icon: 'icon-dashboard'
  },
  {
    path: 'help',
    url: '/help',
    title: '帮助中心',
    icon: 'icon-dashboard',
    children:[
      {
        path: 'problem',
        url: '/problem',
        title: '常见问题',
        icon: 'icon-dashboard'
      },
      {
        path: 'guide',
        url: '/guide',
        title: '使用指南',
        icon: 'icon-dashboard'
      },
    ]
  },
  {
    path: 'charts',
    url: '/charts',
    title: '图表',
    icon: 'icon-dashboard',
    children:[
      {
        path: 'bar',
        url: '/bar',
        title: '柱状图',
        icon: 'icon-dashboard'
      },
      {
        path: 'line',
        url: '/line',
        title: '折线图',
        icon: 'icon-dashboard'
      },
    ]
  },

]