import { defineStore } from "pinia";
// declare interface AsyncRoutesType {
//   path: string,
//   url: string,
//   title: string,
//   icon: string,
//   children?: AsyncRoutesType[]
// }
export const useStore = defineStore('main',{
  state:() => {
    return {
      name:'admin',
      menuList: []
    }
  },
  getters:{
    getName(state) {
      return state.name 
    }
  },
  actions:{
    change(data:string) {
      this.name = data
    }
  }
})