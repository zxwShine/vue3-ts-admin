
import router from './router'


router.beforeEach((to,from,next) => {
  if(to.path === '/') {
    next('/login')
  } else {
    console.log(from,'from')
    next('/home')
  }
})