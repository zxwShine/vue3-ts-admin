# Vue 3 + TypeScript + Vite

前端是由 vue3 + ts + vite + element-plus 构成的，后端用 node 写了一些简易的接口，比如登录接口，退出接口

# 预览图

1、登录页面

![1702956305661](image/README/1702956305661.png)

2、首页

![1702956362650](image/README/1702956362650.png)

3、表格

![1702956415428](image/README/1702956415428.png)

![1702956425784](image/README/1702956425784.png)

4、图表

![1702956449061](image/README/1702956449061.png)

![1702956459492](image/README/1702956459492.png)
