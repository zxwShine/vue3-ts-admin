import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()]
    }),
    Components({
      resolvers: [ElementPlusResolver()]
    }),
  ],
  resolve: {
    alias: {
      '@':  path.resolve('./src')
    }
  },
  server:{
    port: 8080,
    // host: "0.0.0.0", //配置局域网ip访问
    open: true, // 设置服务启动时是否自动打开浏览器
    cors: true,
    proxy:{
      '/api':{
        target: 'http://127.0.0.1:9000',//这是你要跨域请求的地址前缀,9000是server服务的端口，后面改成自己起后台服务的端口
        changeOrigin: true,//开启跨域
        secure: false, // 如果是https接口，需要配置这个参数
        rewrite: path => path.replace(/^\/api/, ''),
      }
    }
  }
})
